package rcmm.pedro.es.game4stats.UI.Fragments;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import rcmm.pedro.es.game4stats.Config.Config;
import rcmm.pedro.es.game4stats.Helper.Thread.PostPlayer;
import rcmm.pedro.es.game4stats.R;
import rcmm.pedro.es.game4stats.UI.Activities.ProfileActivity;
import rcmm.pedro.es.game4stats.UI.Dialogs.NewGameDialog;
import rcmm.pedro.es.game4stats.UI.Dialogs.RegisterDialog;
import rcmm.pedro.es.game4stats.model.Player;


public class LogFragment extends Fragment implements View.OnClickListener{
    @Bind (R.id.username) EditText usernameET;
    @Bind (R.id.password) EditText passwordET;
    @Bind (R.id.btnLog)   Button   btnLog;
    @Bind (R.id.btnReg)   Button   btnReg;
    RegisterDialog registerDialog;
    Player user;
   // private OnFragmentInteractionListener mListener;

    public LogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.fragment_log, container, false);
        ButterKnife.bind(this, v);

        btnLog.setOnClickListener(this);
        btnReg.setOnClickListener(this);
        return v;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
       /* try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case (R.id.btnReg):
                registerRoutine();
                break;
            case (R.id.btnLog):
                loginRoutine();
                break;
        }
    }

    private void loginRoutine() {
        if(checkInput()) {
            user = new Player();
            user.setName(usernameET.getText().toString());
            user.setPassword(passwordET.getText().toString());
            PostPlayer postPlayer = new PostPlayer(user, this, getContext());
            postPlayer.execute(Config.getSERVER_ADDRESS() + "Login", null);
        }
        /**SharedPreferences sharedPreferences = getContext().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        if(sharedPreferences.getString("Token", null)!= null){
            Player p = new Player();
            p.setName(sharedPreferences.getString("UserName", null));

            PostPlayer postPlayer = new PostPlayer(p, this, getContext());
            postPlayer.execute(Config.getSERVER_ADDRESS()+"Login", null);
        }
        Player p = new Player();
        p.setName(usernameET.getText().toString());
        p.setPassword(passwordET.getText().toString());

        PostPlayer postPlayer = new PostPlayer(p, this, getContext());
        postPlayer.execute(Config.getSERVER_ADDRESS()+"Login", null);*/
    }

    private boolean checkInput(){
        return (usernameET.length()>0 && passwordET.length()>0);
    }

    private void registerRoutine() {
        registerDialog = new RegisterDialog(getContext());
        registerDialog.show();
        registerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                goToProfile();
            }
        });
    }

    /***
     * Respuesta a intento de login
     * @param httpResponse
     */
    public void processHttpResponse(String httpResponse) {
        if(httpResponse.equals("noMatchNamePassword")){
            Toast.makeText(getContext(), "Nombre o contraseña incorrectos", Toast.LENGTH_SHORT).show();
        }
        else {
            SharedPreferences sharedPreferences = getContext().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("Token", httpResponse);
            editor.putString("UserName", user.getName());
            editor.apply();
            goToProfile();
        }
    }

    private void goToProfile(){
        /*ProfileFragment profileFragment = new ProfileFragment();
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentContainer, profileFragment).commit();
                */
        Intent intent = new Intent(getContext(), ProfileActivity.class);
        startActivity(intent);
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    /*public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }*/

}
