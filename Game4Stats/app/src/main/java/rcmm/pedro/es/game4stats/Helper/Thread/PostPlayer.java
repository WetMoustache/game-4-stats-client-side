package rcmm.pedro.es.game4stats.Helper.Thread;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.CookieHandler;
import java.net.CookieStore;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import rcmm.pedro.es.game4stats.Helper.Http.httpUrlConnectionUtility;
import rcmm.pedro.es.game4stats.UI.Activities.MainActivity;
import rcmm.pedro.es.game4stats.UI.Dialogs.RegisterDialog;
import rcmm.pedro.es.game4stats.UI.Fragments.LogFragment;
import rcmm.pedro.es.game4stats.UI.Fragments.ProfileFragment;
import rcmm.pedro.es.game4stats.model.Match;
import rcmm.pedro.es.game4stats.model.Player;

/**
 * Created by alpha on 01/02/2016.
 */
public class PostPlayer extends AsyncTask<String, Void, String> {
    HttpURLConnection urlConnection;
    Player player;
    //RegisterDialog dialogCaller;
    //LogFragment fragmentCaller;
    Object caller;
    Context context;

    /*public PostPlayer(Player player, RegisterDialog caller){
        this.player = player;
        this.dialogCaller = caller;
        httpUrlConnectionUtility.getInstance().setContext(dialogCaller.getContext());
        httpUrlConnectionUtility.getInstance().setCookies();
        urlConnection = httpUrlConnectionUtility.getInstance().geturlConnection();
    }

    public PostPlayer(Player player, LogFragment caller){
        this.player = player;
        this.fragmentCaller = caller;
        httpUrlConnectionUtility.getInstance().setContext(fragmentCaller.getContext());
        httpUrlConnectionUtility.getInstance().setCookies();
        urlConnection = httpUrlConnectionUtility.getInstance().geturlConnection();
    }*/

    public PostPlayer(Player player, Object caller, Context context){
        this.player = player;
        this.caller = caller;
        this.context = context;
        httpUrlConnectionUtility.getInstance().setContext(context);
        //httpUrlConnectionUtility.getInstance().setCookies();
        urlConnection = httpUrlConnectionUtility.getInstance().geturlConnection();
    }

    @Override
    protected String doInBackground(String... params) {
        return POST(player, params[0], params[1]);
    }

    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String httpResponse) {
        if(caller instanceof LogFragment) {
            LogFragment logFragment = (LogFragment) caller;
            logFragment.processHttpResponse(httpResponse);
        }
        if(caller instanceof RegisterDialog){
            RegisterDialog registerDialog = (RegisterDialog) caller;
            registerDialog.processResponse(httpResponse);
        }
        if(caller instanceof MainActivity) {
            /*MainActivity mainActivity = (MainActivity) caller;
            mainActivity.processResponse(httpResponse);*/
        }
        if(caller instanceof ProfileFragment){
            ProfileFragment profileFragment = (ProfileFragment) caller;
            profileFragment.processHttpResponse(httpResponse);
        }

    }

    public String POST(Player person, String url, String token){
        String result = "";
        int responseCode = 0;

        try {
            URL urlFormed = new URL (url);

            // 1. create HttpURLConnection
            //HttpURLConnection urlConnection = (HttpURLConnection) urlFormed.openConnection();
            httpUrlConnectionUtility.getInstance().seturlConnection((HttpURLConnection) urlFormed.openConnection());
            urlConnection = httpUrlConnectionUtility.getInstance().geturlConnection();

            // 2. make POST request
            urlConnection.setRequestMethod("POST");

            // 3. set some headers
            if(token != null){
                SharedPreferences sharedPreferences = context.getSharedPreferences("Preferences", Context.MODE_PRIVATE);
                urlConnection.setRequestProperty("Token", sharedPreferences.getString("Token", null));
                urlConnection.setRequestProperty("User", sharedPreferences.getString("UserName", null));
            }
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("section", "postPlayer");



            urlConnection.setDoOutput(true);

            // 4. Add JSON data into POST request body

            // 4.1 Use Jackson object mapper to convert Content object into JSON
            ObjectMapper mapper = new ObjectMapper();

            // 4.2 Get connection output stream
            DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());

            // 4.3 Copy content "JSON into:
            mapper.writeValue(wr, person);
            System.out.println("dataOutputStream---> " + wr.toString());
            // 4.4 send the request
            wr.flush();

            // 4.5 close
            wr.close();

            // 5. Get the response
            responseCode = urlConnection.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + urlFormed);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while((inputLine = in.readLine()) != null){
                response.append(inputLine);
            }
            in.close();
            System.out.println(response.toString());
            return response.toString();
            // 6 Print result


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 7. return result
        return "strangeBehaivour";
    }
}