package rcmm.pedro.es.game4stats.Helper.Thread;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

import rcmm.pedro.es.game4stats.Config.Config;
import rcmm.pedro.es.game4stats.UI.Dialogs.RegisterDialog;

/**
 * Created by alpha on 17/02/2016.
 */
public class GCMUtil extends AsyncTask<Void, Void, String> {

    Context context;
    GoogleCloudMessaging gcm;
    String regid;
    RegisterDialog caller;
    InstanceID instanceID;

    public GCMUtil(Context context, RegisterDialog dialog){
        this.context = context;
        this.caller = dialog;
    }

    @Override
    protected String doInBackground(Void... params) {
        String msg = "";
        try {
            if (instanceID == null) {
                instanceID = InstanceID.getInstance(context);
                //gcm = GoogleCloudMessaging.getInstance(context);
            }
            //regid = gcm.register(Config.getPROJECT_NUMBER());
            regid = instanceID.getToken(Config.getPROJECT_NUMBER(),GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            msg =  regid;
            Log.i("GCM", msg);

        } catch (IOException ex) {
            msg = "Error :" + ex.getMessage();

        }
        return msg;
    }

    @Override
    protected void onPostExecute(String msg) {
        caller.processRegIdResponse(msg);
    }
}
