package rcmm.pedro.es.game4stats.UI.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.List;
import java.util.Set;

import butterknife.Bind;
import butterknife.ButterKnife;
import rcmm.pedro.es.game4stats.Config.Config;
import rcmm.pedro.es.game4stats.Helper.Thread.EncodeImage;
import rcmm.pedro.es.game4stats.Helper.Thread.PostPlayer;
import rcmm.pedro.es.game4stats.R;
import rcmm.pedro.es.game4stats.UI.Dialogs.NewGameDialog;
import rcmm.pedro.es.game4stats.UI.Fragments.LogFragment;
import rcmm.pedro.es.game4stats.UI.Fragments.ProfileFragment;
import rcmm.pedro.es.game4stats.model.Player;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    //@Bind(R.id.fab) FloatingActionButton fab;
    //@Bind(R.id.button) Button btn;
    CookieManager cookieManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        //fab.setOnClickListener(this);
        //btn.setOnClickListener(this);

        if(findViewById(R.id.fragmentContainer)!= null){

            if(savedInstanceState!= null){
                return;
            }

            SharedPreferences sharedPreferences = getSharedPreferences("Preferences", Context.MODE_PRIVATE);
            String token = sharedPreferences.getString("Token", null);

            if(token != null){
                /*ProfileFragment profileFragment = new ProfileFragment();

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragmentContainer, profileFragment).commit();*/
                Intent intent = new Intent(this, ProfileActivity.class);
                startActivity(intent);
            }
            else{
                LogFragment logFragment = new LogFragment();

                getSupportFragmentManager().beginTransaction()
                        .add(R.id.fragmentContainer, logFragment).commit();
            }
        }
    }

    @Override
    public void onClick(View v) {
        /*switch(v.getId()){
            case R.id.fab:
                NewGameDialog newGameDialog = new NewGameDialog(this);
                newGameDialog.show();
                break;
            case R.id.button:
                RegisterUser();
                break;
        }*/
    }

    public void SendImage() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, 0);

    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){
            Uri uri = data.getData();
            String charset = "UTF-8";
            String requestURL = "YOUR_URL";
            String realPath = getRealPathFromURI(this, data.getData());
            EncodeImage encodeImage = new EncodeImage(this, null, realPath);
            encodeImage.execute();

        }
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static String getRealPathFromURI_API19(Context context, Uri uri){
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(uri);

        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];

        String[] column = { MediaStore.Images.Media.DATA };

        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{id}, null);

        int columnIndex = cursor.getColumnIndex(column[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }

    public void onImageEncoded(byte[] bytes) {

    }

    public void setCookies(){
        SharedPreferences sharedPreferences = getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        ObjectMapper mapper = new ObjectMapper();
        if(sharedPreferences.getString("CookieSession","").equals("")){
            Toast.makeText(this, "Cookie NOT in preferences", Toast.LENGTH_SHORT).show();
            cookieManager = new CookieManager();
            try {
                String cookieSessionString = mapper.writeValueAsString(cookieManager);
                Log.e("serializing cookie: ", cookieSessionString);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("CookieSession", cookieSessionString);
                editor.apply();
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        else{
            Toast.makeText(this, "Cookie IN preferences", Toast.LENGTH_SHORT).show();
            try {
                String cookie = sharedPreferences.getString("CookieSession", "");
                Log.e("serialized cookie: ", cookie);
                cookieManager = mapper.readValue(sharedPreferences.getString("CookieSession",""), CookieManager.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        CookieHandler.setDefault(cookieManager);
    }

    /*public void processResponse(String httpResponse) {
        if(!httpResponse.equals("noMatchFound")){

        }
    }*/
}
