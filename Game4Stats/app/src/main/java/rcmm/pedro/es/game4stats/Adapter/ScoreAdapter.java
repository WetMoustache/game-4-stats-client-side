package rcmm.pedro.es.game4stats.Adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;


import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import rcmm.pedro.es.game4stats.R;
import rcmm.pedro.es.game4stats.model.Player;
import rcmm.pedro.es.game4stats.model.PlayersForMatch;

/**
 * Created by alpha on 24/02/2016.
 */
public class ScoreAdapter extends RecyclerView.Adapter<ScoreAdapter.ViewHolder> {

    private PlayersForMatch playersForMatch = PlayersForMatch.getInstance();
    private boolean hideSwipeNumber = false;

    public class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView imageView;
        public TextView nameTextView;
        public EditText swipeNumberPicker;
        public TextView scoreTextView;
        public ViewHolder(View v){
            super (v);
            imageView         = (ImageView)         v.findViewById(R.id.fotoPerfil);
            nameTextView      = (TextView)          v.findViewById(R.id.nombreScore);
            swipeNumberPicker = (EditText) v.findViewById(R.id.number_picker);
            scoreTextView     = (TextView)          v.findViewById(R.id.scoreTextView);

            if(!hideSwipeNumber) {
                swipeNumberPicker.setVisibility(View.VISIBLE);
                scoreTextView.setVisibility(View.INVISIBLE);
                swipeNumberPicker.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if(swipeNumberPicker.getText().length()>0)
                            saveScore(nameTextView.getText().toString(), Integer.parseInt(swipeNumberPicker.getText().toString()));
                    }
                });
               /* swipeNumberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        picker.setValue(newVal);
                        saveScore(nameTextView.getText().toString(), newVal);
                    }
                });*/
                /*swipeNumberPicker.setOnValueChangeListener(new OnValueChangeListener() {
                    @Override
                    public boolean onValueChange(SwipeNumberPicker view, int oldValue, int newValue) {
                        boolean isValueOk = (newValue & 1) == 0;
                        if (isValueOk) {
                            view.setValue(newValue, false);
                            saveScore(nameTextView.getText().toString(), newValue);
                        }
                        return isValueOk;
                    }
                });*/
            }
            else{
                swipeNumberPicker.setVisibility(View.INVISIBLE);
                scoreTextView.setVisibility(View.VISIBLE);
            }
        }
    }

    private void saveScore(String name, int newValue) {
        int index = getPlayerIndex(name);
        playersForMatch.getPlayerScore().set(index, newValue);
    }

    public void hideScoreSwipper(){
        hideSwipeNumber = true;
    }

    private int getPlayerIndex(String name){
        boolean enc = false;
        int index = 0;
        for(int i=0; i <playersForMatch.getPlayerList().size() && !enc ;i++){
            if(playersForMatch.getPlayerList().get(i).getName().equals(name)){
                enc = true;
                index = i;
            }
        }
        return index;
    }

    public void add(int position, Player item){
        playersForMatch.getPlayerList().add(position, item);
        notifyItemInserted(position);
    }

    public void remove (Player item){
        int position = playersForMatch.getPlayerList().indexOf(item);
        playersForMatch.getPlayerList().remove(position);
        notifyItemRemoved(position);
    }

    public ScoreAdapter (boolean hideSwipeNumber){
        for(int i =0; i<playersForMatch.getPlayerList().size(); i++){
            playersForMatch.getPlayerScore().add(0);
        }
        if(hideSwipeNumber)
            hideScoreSwipper();
    }

    @Override
    public ScoreAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_score, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position){
        byte[] image = null;
        boolean hasImage = false;
        final String name = playersForMatch.getPlayerList().get(position).getName();

        holder.nameTextView.setText(name);
        holder.scoreTextView.setText(playersForMatch.getPlayerScore().get(position).toString());

        if(playersForMatch.getPlayerList().get(position).getEncodedImage().length>1) {
            image = playersForMatch.getPlayerList().get(position).getEncodedImage();
            hasImage = true;
        }
        final Long id = playersForMatch.getPlayerList().get(position).getIdPlayer();
        if(hasImage) {
            ByteArrayInputStream arrayInputStream = new ByteArrayInputStream(image);
            Bitmap decodedBitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
            holder.imageView.setImageBitmap(decodedBitmap);
        }
        holder.swipeNumberPicker.setText(Integer.toString(scoreSaved(name)));
    }

    private int scoreSaved(String name) {
        return playersForMatch.getPlayerScore().get(getPlayerIndex(name));
    }

    @Override
    public int getItemCount() {
        return playersForMatch.getPlayerList().size();
    }
}
