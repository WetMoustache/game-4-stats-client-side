package rcmm.pedro.es.game4stats.Adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import rcmm.pedro.es.game4stats.model.Match;
import rcmm.pedro.es.game4stats.model.Player;

import rcmm.pedro.es.game4stats.R;
import rcmm.pedro.es.game4stats.model.PlayersForMatch;

/**
 * Created by alpha on 01/02/2016.
 */
public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.ViewHolder> {
    private List<Player> items;
    private PlayersForMatch playersForMatch = PlayersForMatch.getInstance();

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView textView;
        public ImageView imageView;
        public LinearLayout linearLayoutholder;
        public Long identifier;

        public ViewHolder(View v){
            super (v);
            textView = (TextView) v.findViewById(R.id.nombre);
            imageView = (ImageView) v.findViewById(R.id.fotoPerfil);
            linearLayoutholder = (LinearLayout) v.findViewById(R.id.viewholderlayout);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view){
            Player player = new Player();
            player.setName(textView.getText().toString());
            player.setEncodedImage(getImageInBytes(imageView));
            ArrayList<Player> newPlayerList = playersForMatch.getPlayerList();
            if(!isSelected(player.getName())) {
                //player.setEncodedImage(getImageInBytes(imageView));
                newPlayerList.add(player);
                playersForMatch.setPlayerList(newPlayerList);
                imageView.setAlpha((float) 1);
            }else{
                playersForMatch.remove(player.getName());
                imageView.setAlpha((float) 0.5);
            }
        }
    }

    public void add(int position, Player item){
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void remove (Player item){
        int position = items.indexOf(item);
        items.remove(position);
        notifyItemRemoved(position);
    }

    public FriendsAdapter (List<Player> mItems){
        this.items = mItems;
    }

    @Override
    public FriendsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_player, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position){
        final String name = items.get(position).getName();
        byte[] image = null;
        boolean hasImage = false;
        if(items.get(position).getEncodedImage().length>1) {
            image = items.get(position).getEncodedImage();
            hasImage = true;
        }
        final Long id = items.get(position).getIdPlayer();
        if(hasImage) {
            ByteArrayInputStream arrayInputStream = new ByteArrayInputStream(image);
            Bitmap decodedBitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
            holder.imageView.setImageBitmap(decodedBitmap);
        }


        holder.identifier = id;
        holder.textView.setText(name);

        ImageView photo = holder.imageView;
                if(isSelected(holder.textView.getText().toString())) {
                    //newPlayerList.add(player);
                    //playersForMatch.setPlayerList(newPlayerList);
                    photo.setAlpha((float) 1);
                }else{
                    //playersForMatch.remove(player.getName());
                    photo.setAlpha((float) 0.5);
                }
            }
      //  });*/
    //}

    private boolean isSelected(String name){
        boolean enc = false;
        for(int i =0; i<playersForMatch.getPlayerList().size() && !enc ;i++){
            if(playersForMatch.getPlayerList().get(i).getName()==name){
                enc = true;
            }
        }
        return enc;
    }

    private byte[] getImageInBytes(ImageView photo) {
        photo.setDrawingCacheEnabled(true);
        photo.buildDrawingCache();
        Bitmap bitmap = photo.getDrawingCache();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        System.out.println(stream);
        return stream.toByteArray();
    }

    @Override
    public int getItemCount(){
        return items.size();
    }
}
