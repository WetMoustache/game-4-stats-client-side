package rcmm.pedro.es.game4stats.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown=true)


public class Player {
    private Long idPlayer;
    private String name;
    private String email;
    private String password;
    private byte[] encodedImage;
    private String gcmId;
    private String token;
    private boolean isClicked;


    public boolean isClicked() {
        return isClicked;
    }

    public void setIsClicked(boolean isClicked) {
        this.isClicked = isClicked;
    }

    public Player (Long idPlayer, String name, String password, String email){
        this.idPlayer = idPlayer;
        this.name = name;
        this.password = password;
        this.email = email;

    }

    public Player (Long idPlayer, String name, String password, String email, byte[] encodedImage, String regId, String gcmid){
        this.idPlayer = idPlayer;
        this.name = name;
        this.password = password;
        this.email = email;
        this.encodedImage = encodedImage;
        this.gcmId = gcmid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Player(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getIdPlayer() {
        return idPlayer;
    }

    public void setIdPlayer(Long id) {
        this.idPlayer = id;
    }

    public byte[] getEncodedImage() {
        if(encodedImage==null)
            this.encodedImage = new byte[0];
        return encodedImage;
    }

    public void setEncodedImage(byte[] encodedImage) {
        this.encodedImage = encodedImage;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
    public String getGcmId() {
        return gcmId;
    }

    public void setGcmId(String gcmId) {
        this.gcmId = gcmId;
    }

    public boolean isImageLoaded(){
        return this.encodedImage != null;
    }
}
