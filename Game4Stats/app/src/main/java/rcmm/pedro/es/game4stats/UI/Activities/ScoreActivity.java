package rcmm.pedro.es.game4stats.UI.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import rcmm.pedro.es.game4stats.R;
import rcmm.pedro.es.game4stats.UI.Fragments.ScoreFragment;

public class ScoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        ScoreFragment scoreFragment = new ScoreFragment();

        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragmentContainer, scoreFragment).commit();
    }
}
