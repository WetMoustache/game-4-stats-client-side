package rcmm.pedro.es.game4stats.Helper.Thread;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.provider.Settings;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import rcmm.pedro.es.game4stats.UI.Dialogs.NewGameDialog;
import rcmm.pedro.es.game4stats.model.Game;
import rcmm.pedro.es.game4stats.model.Match;

/**
 * Created by alpha on 01/02/2016.
 */
public class LoadItems extends AsyncTask<String, Void, String> {
    private List<Settings.NameValueTable> params;
    private NewGameDialog dialog;
    private List<Game> listGame;
    private String item;
    private Context context;

    public LoadItems (List<Settings.NameValueTable> params, NewGameDialog dialog, String item){
        this.params = params;
        this.dialog = dialog;
        this.item   = item;
        this.context = dialog.getContext();
    }

    @Override
    protected void onPreExecute(){
        listGame = new ArrayList<>();
    }
    @Override
    protected String doInBackground(String... urls) {
        return POST(urls[0], item, urls[1]);
    }
    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String jsonResult) {
        Toast.makeText(dialog.getContext(), "Data Sent!", Toast.LENGTH_LONG).show();
        if(item.equals("games"))
            dialog.populateSpinner(jsonResult);
        if(item.equals("players"))
            dialog.populateRecyclerView(jsonResult);
    }

    public String POST(String url, String item, String token){
        String result = "";
        ArrayList<String> listItems = new ArrayList<>();
        StringBuffer jsonResponse = new StringBuffer();


        try {
            URL urlFormed = new URL (url);

            // 1. create HttpURLConnection
            HttpURLConnection urlConnection = (HttpURLConnection) urlFormed.openConnection();

            // 2. make GET request
            urlConnection.setRequestMethod("POST");

            // 3. set some headers

            if(token != null){
                SharedPreferences sharedPreferences = context.getSharedPreferences("Preferences", Context.MODE_PRIVATE);
                urlConnection.setRequestProperty("Token", sharedPreferences.getString("Token", null));
                urlConnection.setRequestProperty("User", sharedPreferences.getString("UserName", null));
            }

            if(item.equals("games"))
                urlConnection.setRequestProperty("section", "getGames");
            if(item.equals("players"))
                urlConnection.setRequestProperty("section", "getPlayers");

            // 5. Get the response

            int responseCode = urlConnection.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + urlFormed);
            System.out.println("Response Code : " + responseCode);

            /************************
             *
             * RECIBIR UN ARRAY DE JSONS DE ALGÚN MODO Y AVERIGUAR COMO CONVERTIRLO EN LA LISTA QUE DEBE MOSTRAR EL DIÁLOGO
             * ¡¡ACUÉRDATE DE QUE QUIERES HACER QUE EL INPUT RECIBIDO SEA GENÉRICO PARA PODER RECIBIR TANTO JUGADORES COMO
             * JUEGOS!!
             */
            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String inputLine;

            while((inputLine = in.readLine()) != null){
                jsonResponse.append(inputLine);
            }
            in.close();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 7. return result
        return jsonResponse.toString();
    }
}
