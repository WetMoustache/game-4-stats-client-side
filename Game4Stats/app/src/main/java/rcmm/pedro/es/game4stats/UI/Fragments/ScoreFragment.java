package rcmm.pedro.es.game4stats.UI.Fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;


import butterknife.Bind;
import butterknife.ButterKnife;
import rcmm.pedro.es.game4stats.Adapter.ScoreAdapter;
import rcmm.pedro.es.game4stats.Config.Config;
import rcmm.pedro.es.game4stats.Helper.Thread.PostMatch;
import rcmm.pedro.es.game4stats.R;


public class ScoreFragment extends Fragment implements View.OnClickListener{

    private RecyclerView recyclerView;
    private RecyclerView.Adapter scoreAdapter;
    private RecyclerView.LayoutManager layoutManager;
    @Bind (R.id.acceptGameButton) FloatingActionButton acceptFab;
    @Bind (R.id.rejectGameButton) FloatingActionButton rejectFab;

    public ScoreFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_score, container, false);
        ButterKnife.bind(this, v);
        recyclerView = (RecyclerView) v.findViewById(R.id.scoreRecycler);
        recyclerView.setHasFixedSize(true);
        //layoutManager = new GridLayoutManager(getContext(), 4);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        scoreAdapter = new ScoreAdapter(true);
        recyclerView.setAdapter(scoreAdapter);
        acceptFab.setOnClickListener(this);
        rejectFab.setOnClickListener(this);
        return v;

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case(R.id.acceptGameButton):
                PostMatch postAcceptMatch = new PostMatch(this, getContext());
                postAcceptMatch.execute(Config.getSERVER_ADDRESS() + "PostMatch/Accept", "yes");
                break;
            case(R.id.rejectGameButton):
                PostMatch postRejectMatch = new PostMatch(this, getContext());
                postRejectMatch.execute(Config.getSERVER_ADDRESS() + "PostMatch/Reject", "yes");
                break;
        }
    }

    public void proccessHttpResponse(String httpResponse) {

    }
}
