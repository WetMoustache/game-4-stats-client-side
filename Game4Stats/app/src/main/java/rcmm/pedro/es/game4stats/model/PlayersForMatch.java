package rcmm.pedro.es.game4stats.model;

import java.util.ArrayList;

/**
 * Created by alpha on 24/02/2016.
 */
public class PlayersForMatch {

    private static PlayersForMatch INSTANCE = null;
    private ArrayList<Player> playerList;
    private ArrayList<Integer> playerScore;
    private Game game;
    private String creatorName;
    private String timestamp;


    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public ArrayList<Integer> getPlayerScore() {
        return playerScore;
    }
    public void setPlayerScore(ArrayList<Integer> playerScore) {
        this.playerScore = playerScore;
    }



    private PlayersForMatch(){

        playerList  = new ArrayList<>();
        playerScore = new ArrayList<>();
        game = new Game();
    }

    private static void createInstance(){
        if(INSTANCE == null){
            synchronized (PlayersForMatch.class){
                if(INSTANCE == null)
                    INSTANCE = new PlayersForMatch();
            }
        }
    }

    public static PlayersForMatch getInstance(){
        if(INSTANCE == null){
            createInstance();
        }
        return INSTANCE;
    }

    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    public ArrayList<Player> getPlayerList() {
        return playerList;
    }

    public void setPlayerList(ArrayList<Player> playerList) {
        this.playerList = playerList;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public void remove(String name) {
        boolean enc = false;
        for(int i =0; i <playerList.size() && !enc; i++){
            if(playerList.get(i).getName().equals(name)) {
                playerList.remove(i);
                enc = true;
            }
        }
    }
}
