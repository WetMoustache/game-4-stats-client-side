package rcmm.pedro.es.game4stats.Config;

/**
 * Created by alpha on 01/02/2016.
 */
public class Config {
    static String SERVER_ADDRESS = "http://192.168.1.36:8080/game4statisticsserver/";
    static String IMAGE_SERVLET = "uploadImage";
    static String PROJECT_NUMBER = "195791874234";

    public static String getSERVER_ADDRESS() {return SERVER_ADDRESS;}
    public static String getIMAGE_SERVLET(){return IMAGE_SERVLET;}
    public static String getPROJECT_NUMBER(){return PROJECT_NUMBER;}
    public static int getNOTIFICATION_ID(){return (int) System.currentTimeMillis();}
}
