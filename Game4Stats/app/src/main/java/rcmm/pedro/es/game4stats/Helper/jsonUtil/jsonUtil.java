package rcmm.pedro.es.game4stats.Helper.jsonUtil;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;

import rcmm.pedro.es.game4stats.model.Player;
import rcmm.pedro.es.game4stats.model.PlayersForMatch;

/**
 * Created by alpha on 19/02/2016.
 */
public class jsonUtil {
    public static Player jsonToPlayer(InputStream stream) throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        Player model = mapper.readValue(stream, Player.class);
        return model;
    }

    public static Player jsonToPlayer(String stream){
        ObjectMapper mapper = new ObjectMapper();
        Player model = null;
        try {
            model = mapper.readValue(stream, Player.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return model;
    }

    public static String playerToJson(Player p){
        ObjectMapper mapper = new ObjectMapper();
        String json;
        try {
            json = mapper.writeValueAsString(p);
            return json;
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.out.println("unable to parse player to json");
        }
        return null;
    }

    public static PlayersForMatch jsonToPlayersForMatch(InputStream stream){
        ObjectMapper mapper = new ObjectMapper();
        PlayersForMatch model = null;
        try {
            model = mapper.readValue(stream, PlayersForMatch.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return model;
    }
}
