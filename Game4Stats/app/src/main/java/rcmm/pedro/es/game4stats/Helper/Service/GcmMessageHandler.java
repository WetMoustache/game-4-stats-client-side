package rcmm.pedro.es.game4stats.Helper.Service;

/**
 * Created by alpha on 17/02/2016.
 */
import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.annotation.TargetApi;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import rcmm.pedro.es.game4stats.Adapter.ScoreAdapter;
import rcmm.pedro.es.game4stats.Config.Config;
import rcmm.pedro.es.game4stats.Helper.BroadcastReceiver.GcmBroadcastReceiver;
import rcmm.pedro.es.game4stats.Helper.jsonUtil.jsonUtil;
import rcmm.pedro.es.game4stats.R;
import rcmm.pedro.es.game4stats.UI.Activities.ProfileActivity;
import rcmm.pedro.es.game4stats.UI.Activities.ScoreActivity;
import rcmm.pedro.es.game4stats.model.Player;
import rcmm.pedro.es.game4stats.model.PlayersForMatch;

public class GcmMessageHandler extends IntentService {

    String message;
    Notification notification;
    private Handler handler;
    public GcmMessageHandler() {
        super("GcmMessageHandler");
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        handler = new Handler();
    }
    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();

        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);
        message   = extras.getString("message");

        Log.i("GCM", "Received : (" + messageType + ")  " + message);
        if(message.startsWith("MatchScore")){
                proccessMatchScore(message);
                //char[] messageChars = message.toCharArray();
               // InputStream stream = new ByteArrayInputStream(playerString.getBytes(StandardCharsets.UTF_8));
                //PlayersForMatch playersForMatch = jsonUtil.jsonToPlayersForMatch(stream);
                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                buildNotification(PlayersForMatch.getInstance(), false);

                notificationManager.notify(Config.getNOTIFICATION_ID(), notification);
        }
        else
            if(message.startsWith("MatchCanceled")){
                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                buildNotification(PlayersForMatch.getInstance(), true);

                notificationManager.notify(Config.getNOTIFICATION_ID(), notification);
            }
        //showToast();

        GcmBroadcastReceiver.completeWakefulIntent(intent);

    }

    private void proccessMatchScore(String message) {
        proccessScore(message);
        int indexOfFiled = message.indexOf("*")+1;
        String players = message.substring(indexOfFiled, message.indexOf("*", indexOfFiled));
        indexOfFiled = message.indexOf("*", indexOfFiled);
        String idGame = message.substring(indexOfFiled+1, message.indexOf("*", indexOfFiled+1));
        indexOfFiled = message.indexOf("*", indexOfFiled+1);
        String creatorName = message.substring(indexOfFiled+1, message.indexOf("*", indexOfFiled+1));
        indexOfFiled = message.indexOf("*", indexOfFiled + 1);

        String timeStamp = message.substring(indexOfFiled+1, message.length());
        PlayersForMatch.getInstance().setTimestamp(timeStamp);
        PlayersForMatch.getInstance().getGame().setId(Long.parseLong(idGame));
        PlayersForMatch.getInstance().setCreatorName(creatorName);
        proccessPlayers(players);
    }

    private void proccessScore(String message){
        boolean out = false;
        String score     = message.substring(message.indexOf("[")+1, message.indexOf("]"));
        int index = score.indexOf(",");
        int prevIndex = 0;
        String subScore  = score.substring  (prevIndex, index);

        PlayersForMatch.getInstance().getPlayerScore().removeAll(new ArrayList<Integer>());
        for(int i=0; i<score.length() && !out; i++){
            PlayersForMatch.getInstance().getPlayerScore().add(i, Integer.parseInt(subScore));
            prevIndex = index;
            index = score.indexOf(",", index+1);
            if(index > -1) {
                subScore = score.substring(prevIndex+2, index);
            }else{
                subScore = score.substring(prevIndex+2, score.length()-1);
                out = true;
            }
        }
    }

    private void proccessPlayers(String message){
        boolean out = false;
        String players = message.substring(message.indexOf("[") + 1, message.indexOf("]"));
        int index = players.indexOf(",");
        int prevIndex = 0;
        String subPlayer  = players.substring(prevIndex, index);

        PlayersForMatch.getInstance().getPlayerList().removeAll(new ArrayList<Player>());
        for(int i=0; i<players.length() && !out; i++){
            Player p = new Player();
            p.setName(subPlayer);
            PlayersForMatch.getInstance().getPlayerList().add(i, p);
            prevIndex = index;
            index = players.indexOf(",", index+1);
            if(index > -1) {
                subPlayer = players.substring(prevIndex+2, index);
            }else{
                subPlayer = players.substring(prevIndex+2, players.length()-1);
                out = true;
            }
        }
    }

    private NotificationCompat.Builder buildNotification(PlayersForMatch playersForMatch, boolean canceled) {
        NotificationCompat.Builder builder;
        Intent intent = new Intent(this, ScoreActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, Config.getNOTIFICATION_ID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // This ensures that the back button follows the recommended
        // convention for the back key.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(ProfileActivity.class);

        if(!canceled) {
            builder = new NotificationCompat.Builder(getApplicationContext())
                    .setSmallIcon(R.drawable.register_icon)
                    .setTicker(getApplicationContext().getString(R.string.customnotificationticker))
                    .setAutoCancel(true)
                    .setContentIntent(pIntent);
        }else{
            builder = new NotificationCompat.Builder(getApplicationContext())
                    .setSmallIcon(R.drawable.statsicon)
                    .setTicker(getApplicationContext().getString(R.string.customnotificationticker))
                    .setAutoCancel(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            RemoteViews expandedView = new RemoteViews(this.getPackageName(),
                    R.layout.custom_notification);
            expandedView.setImageViewResource(R.id.imagenotileft, R.mipmap.ic_user);
            notification = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.drawer_toggle)
                    .setAutoCancel(true)
                    .setContentIntent(pIntent)
                    .setContentTitle("Nuevo resultado a la espera de aprobación").build();
            notification.contentView = expandedView;
            notification.bigContentView = expandedView;

            return null;
        } else {
            notification = new NotificationCompat.Builder(this)
                    .setContentTitle(playersForMatch.getGame().getNombre())
                    .setContentText("VERSION JELLY BEANS")
                    .setSmallIcon(android.R.drawable.ic_menu_gallery).build();
        }

        return builder;
    }

    public void showToast(){
        handler.post(new Runnable() {
            public void run() {
                Toast.makeText(getApplicationContext(), "eooo", Toast.LENGTH_LONG).show();
            }
        });
    }

    private RemoteViews getComplexNotificationView() {
        // Using RemoteViews to bind custom layouts into Notification
        RemoteViews notificationView = new RemoteViews(
                getApplicationContext().getPackageName(),
                R.layout.custom_notification);

        // Locate and set the Image into customnotificationtext.xml ImageViews
        notificationView.setImageViewResource(R.id.imagenotileft, R.mipmap.ic_launcher);

         //Locate and set the Text into customnotificationtext.xml TextViews
        notificationView.setTextViewText(R.id.title, PlayersForMatch.getInstance().getGame().getNombre());
        notificationView.setTextViewText(R.id.text, "puntuación");


        return notificationView;

    }
}
