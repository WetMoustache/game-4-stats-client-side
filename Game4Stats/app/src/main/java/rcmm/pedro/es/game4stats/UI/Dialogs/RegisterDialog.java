package rcmm.pedro.es.game4stats.UI.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import rcmm.pedro.es.game4stats.Config.Config;
import rcmm.pedro.es.game4stats.Helper.Thread.GCMUtil;
import rcmm.pedro.es.game4stats.Helper.Thread.PostPlayer;
import rcmm.pedro.es.game4stats.Helper.jsonUtil.jsonUtil;
import rcmm.pedro.es.game4stats.R;
import rcmm.pedro.es.game4stats.model.Player;

/**
 * Created by alpha on 02/02/2016.
 */

public class RegisterDialog extends Dialog implements View.OnClickListener{
    @Bind (R.id.registerFab) FloatingActionButton fab;
    @Bind (R.id.username)    EditText             usernameET;
    @Bind (R.id.email)       EditText             emailET;
    @Bind (R.id.password)    EditText             passwordET;
    private int httpResponseCode;
    Player p;
    String token;
    String regId;

    public RegisterDialog(Context context) {
        super(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        setContentView(R.layout.dialog_register);
        p = new Player();
        ButterKnife.bind(this);
        fab.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==fab.getId()){
            if(checkData()){
                p.setName(usernameET.getText().toString());
                p.setPassword(passwordET.getText().toString());
                p.setEmail(emailET.getText().toString());
                SharedPreferences sharedPreferences = getContext().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
                if(sharedPreferences.getString("regId", null)!= null) {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("regId", null);
                }
                GCMUtil gcmUtil = new GCMUtil(getContext(), this);
                gcmUtil.execute();
            }
        }
    }

    boolean checkData(){
        return (usernameET.length()>0 && passwordET.length()>0 && emailET.length()>0);
    }

    /***
     * Respuesta a intento de registro
     * @param httpResponse
     */
    public void processResponse(String httpResponse){
        switch(httpResponse){
            case("duplicated"):
                Toast.makeText(getContext(), "Ya existe un usuario con ese nombre o email, deben ser únicos", Toast.LENGTH_SHORT).show();
                break;
            default:
                Player p = jsonUtil.jsonToPlayer(httpResponse);
                SharedPreferences sharedPreferences = getContext().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("Token", p.getToken());
                editor.putString("UserName", p.getName());
                editor.putString("RegId", regId);
                editor.apply();
                this.dismiss();
                break;
        }

    }

    public void processRegIdResponse(String regId) {
        if(regId.startsWith("Error")){
            Toast.makeText(getContext(), "Error de registro, inténtalo más tarde", Toast.LENGTH_SHORT).show();
        }
        else {
            p.setGcmId(regId);
            PostPlayer postPlayer = new PostPlayer(p, this, getContext());
            postPlayer.execute(Config.getSERVER_ADDRESS() + "Register", "yes");
        }
    }
}
