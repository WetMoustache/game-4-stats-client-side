package rcmm.pedro.es.game4stats.UI.Activities;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.fasterxml.jackson.databind.ObjectMapper;

import rcmm.pedro.es.game4stats.Config.Config;
import rcmm.pedro.es.game4stats.R;
import rcmm.pedro.es.game4stats.model.Match;

public class POSTActivity extends AppCompatActivity implements OnClickListener {

        TextView tvIsConnected;
        EditText etName,etCountry,etTwitter;
        Button btnPost;

        Match match;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_post);

            // get reference to the views
            tvIsConnected = (TextView) findViewById(R.id.tvIsConnected);
            etName = (EditText) findViewById(R.id.etName);
            etCountry = (EditText) findViewById(R.id.etCountry);
            etTwitter = (EditText) findViewById(R.id.etTwitter);
            btnPost = (Button) findViewById(R.id.btnPost);

            // check if you are connected or not
            if(isConnected()){
                tvIsConnected.setBackgroundColor(0xFF00CC00);
                tvIsConnected.setText("You are conncted");
            }
            else{
                tvIsConnected.setText("You are NOT conncted");
            }

            // add click listener to Button "POST"
            btnPost.setOnClickListener(this);

        }

        public static String POST(String url, Match person){
            String result = "";

            try {
                URL urlFormed = new URL (url);

                // 1. create HttpURLConnection
                HttpURLConnection urlConnection = (HttpURLConnection) urlFormed.openConnection();

                // 2. make POST request
                urlConnection.setRequestMethod("POST");

                // 3. set some headers
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestProperty("section", "postGame");

                urlConnection.setDoOutput(true);

                // 4. Add JSON data into POST request body

                // 4.1 Use Jackson object mapper to convert Content object into JSON
                ObjectMapper mapper = new ObjectMapper();

                // 4.2 Get connection output stream
                DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());

                // 4.3 Copy content "JSON into:
                mapper.writeValue(wr, person);

                // 4.4 send the request
                wr.flush();

                // 4.5 close
                wr.close();

                // 5. Get the response

                int responseCode = urlConnection.getResponseCode();
                System.out.println("\nSending 'POST' request to URL : " + urlFormed);
                System.out.println("Response Code : " + responseCode);

                BufferedReader in = new BufferedReader(new InputStreamReader (urlConnection.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while((inputLine = in.readLine()) != null){
                    response.append(inputLine);
                }
                in.close();

                // 6 Print result
                System.out.println(response.toString());

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            // 7. return result
            return result;
        }

        public boolean isConnected(){
            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected())
                return true;
            else
                return false;
        }
        @Override
        public void onClick(View view) {

            switch(view.getId()){
                case R.id.btnPost:
                    if(!validate())
                        Toast.makeText(getBaseContext(), "Enter some data!", Toast.LENGTH_LONG).show();
                    // call AsynTask to perform network operation on separate thread
                    new HttpAsyncTask().execute(Config.getSERVER_ADDRESS()+"GCMNotification");
                    break;
            }

        }
        private class HttpAsyncTask extends AsyncTask<String, Void, String> {

            @Override
            protected void onPreExecute(){
                match = new Match();
                setGame();
            }
            @Override
            protected String doInBackground(String... urls) {
                return POST(urls[0], match);
            }
            // onPostExecute displays the results of the AsyncTask.
            @Override
            protected void onPostExecute(String result) {
                Toast.makeText(getBaseContext(), "Data Sent!", Toast.LENGTH_LONG).show();
            }
        }

    private void setGame() {
        match.setIdJ1((long) 1);
        match.setIdJ2((long) 2);
        match.setIdJ3((long) 3);
        match.setIdJ4((long) 4);
        match.setIdJ5((long) 5);
        match.setIdJ6((long) 6);
        match.setIdJ7((long) 7);
        match.setIdJ8((long) 8);
        match.setIdJuego((long) 1);
        match.setIdPartida((long) 1);
    }

    private boolean validate(){
            if(etName.getText().toString().trim().equals(""))
                return false;
            else if(etCountry.getText().toString().trim().equals(""))
                return false;
            else if(etTwitter.getText().toString().trim().equals(""))
                return false;
            else
                return true;
        }
        private static String convertInputStreamToString(InputStream inputStream) throws IOException{
            BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
            String line = "";
            String result = "";
            while((line = bufferedReader.readLine()) != null)
                result += line;

            inputStream.close();
            return result;

        }

}
