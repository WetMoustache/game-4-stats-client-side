package rcmm.pedro.es.game4stats.model;

/**
 * Created by alpha on 01/02/2016.
 */
public class Game {

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private Long id;

    private String nombre;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
