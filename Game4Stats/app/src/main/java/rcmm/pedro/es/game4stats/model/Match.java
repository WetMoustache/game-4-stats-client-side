package rcmm.pedro.es.game4stats.model;
/**
 * Created by alpha on 30/01/2016.
 */

public class Match {

    private Long idPartida, idJuego, idJ1, idJ2, idJ3, idJ4, idJ5, idJ6, idJ7, idJ8;

    public Long getIdPartida() {
        return idPartida;
    }

    public void setIdPartida(Long idPartida) {
        this.idPartida = idPartida;
    }

    public Long getIdJuego() {
        return idJuego;
    }

    public void setIdJuego(Long idJuego) {
        this.idJuego = idJuego;
    }

    public Long getIdJ1() {
        return idJ1;
    }

    public void setIdJ1(Long idJ1) {
        this.idJ1 = idJ1;
    }

    public Long getIdJ2() {
        return idJ2;
    }

    public void setIdJ2(Long idJ2) {
        this.idJ2 = idJ2;
    }

    public Long getIdJ3() {
        return idJ3;
    }

    public void setIdJ3(Long idJ3) {
        this.idJ3 = idJ3;
    }

    public Long getIdJ4() {
        return idJ4;
    }

    public void setIdJ4(Long idJ4) {
        this.idJ4 = idJ4;
    }

    public Long getIdJ5() {
        return idJ5;
    }

    public void setIdJ5(Long idJ5) {
        this.idJ5 = idJ5;
    }

    public Long getIdJ6() {
        return idJ6;
    }

    public void setIdJ6(Long idJ6) {
        this.idJ6 = idJ6;
    }

    public Long getIdJ7() {
        return idJ7;
    }

    public void setIdJ7(Long idJ7) {
        this.idJ7 = idJ7;
    }

    public Long getIdJ8() {
        return idJ8;
    }

    public void setIdJ8(Long idJ8) {
        this.idJ8 = idJ8;
    }

    public Match(){}



}