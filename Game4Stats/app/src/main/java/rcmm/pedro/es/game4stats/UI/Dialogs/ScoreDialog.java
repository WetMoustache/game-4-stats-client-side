package rcmm.pedro.es.game4stats.UI.Dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import rcmm.pedro.es.game4stats.Adapter.ScoreAdapter;
import rcmm.pedro.es.game4stats.Config.Config;
import rcmm.pedro.es.game4stats.Helper.Thread.LoadItems;
import rcmm.pedro.es.game4stats.Helper.Thread.PostMatch;
import rcmm.pedro.es.game4stats.R;

/**
 * Created by alpha on 24/02/2016.
 */
public class ScoreDialog extends Dialog implements View.OnClickListener{
    Activity activity;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter scoreAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private FloatingActionButton submitScoreFab;

    public ScoreDialog(Activity activity){
        super(activity);
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_score);

        submitScoreFab = (FloatingActionButton) findViewById(R.id.submitFullScore);

        recyclerView = (RecyclerView) findViewById(R.id.scoreRecycler);
        recyclerView.setHasFixedSize(true);
        //layoutManager = new GridLayoutManager(getContext(), 4);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        scoreAdapter = new ScoreAdapter(false);
        recyclerView.setAdapter(scoreAdapter);
        submitScoreFab.setOnClickListener(this);

    }

    public void proccessHttpResposne(String httpResponse) {
        if(httpResponse=="ERROR")
            Toast.makeText(this.activity, "ERROR", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == submitScoreFab.getId()) {
            PostMatch postMatch = new PostMatch(this, getContext());
            postMatch.execute(Config.getSERVER_ADDRESS() + "PostMatch", "yes");
        }
    }
}
