package rcmm.pedro.es.game4stats.Helper.Thread;

import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.view.View;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import rcmm.pedro.es.game4stats.Config.Config;
import rcmm.pedro.es.game4stats.Helper.Http.MultipartUtility;
import rcmm.pedro.es.game4stats.UI.Activities.MainActivity;

public class EncodeImage extends AsyncTask<Void, Integer, byte[]> {

    public ContentResolver contentResolver;
    public String uri;
    public MainActivity activity;

    public EncodeImage(MainActivity activity, ContentResolver contentResolver, String uri) {
        this.activity = activity;
        this.uri = uri;
        this.contentResolver = contentResolver;
    }

    @Override
    protected void onPreExecute(){

    }

    @Override
    protected byte[] doInBackground(Void... params) {
        try {
            MultipartUtility multipart = new MultipartUtility(Config.getSERVER_ADDRESS()+ Config.getIMAGE_SERVLET(), "UTF-8");
            multipart.addHeaderField("userName", "Peter");
            multipart.addFormField("userName", "Peter");
            multipart.addFilePart("file", new File(uri));
            List<String> response = multipart.finish(); // response from server.
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = contentResolver.query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

}