package rcmm.pedro.es.game4stats.UI.Dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import rcmm.pedro.es.game4stats.Adapter.FriendsAdapter;
import rcmm.pedro.es.game4stats.Config.Config;
import rcmm.pedro.es.game4stats.Helper.Thread.LoadItems;
import rcmm.pedro.es.game4stats.R;
import rcmm.pedro.es.game4stats.model.Game;
import rcmm.pedro.es.game4stats.model.Player;
import rcmm.pedro.es.game4stats.model.PlayersForMatch;

/**
 * Created by alpha on 01/02/2016.
 */
public class NewGameDialog extends Dialog implements View.OnClickListener{
    public Activity c;
    private Spinner gameSpinner;
    private ArrayAdapter<String> gameAdapter;
    private List<Game> gameList;
    private List<Player> playerList;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter playerAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private FloatingActionButton submitButton;

    public NewGameDialog(Activity a){
        super(a);
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_new_game);
        gameSpinner = (Spinner) findViewById(R.id.games_spinner);
        submitButton = (FloatingActionButton) findViewById(R.id.insertGamePlayers);

        playerList = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.playersRecycler);
        recyclerView.setHasFixedSize(true);
        //layoutManager = new GridLayoutManager(getContext(), 4);
        layoutManager = new GridLayoutManager(getContext(), 2, GridLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);

        //playerAdapter = new FriendsAdapter(playerList);
        //recyclerView.setAdapter(playerAdapter);

        LoadItems loadPlayers = new LoadItems(null, this, "players");
        loadPlayers.execute(Config.getSERVER_ADDRESS()+"GetPlayers", "yes");

        LoadItems loadGames = new LoadItems(null, this, "games");
        loadGames.execute(Config.getSERVER_ADDRESS()+"GetGames", "yes");
        submitButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case (R.id.insertGamePlayers):
                if(checkItemsSelected())
                    dismiss();
                else
                    Toast.makeText(getContext(), "Selecciona los jugadores y el juego", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public boolean checkItemsSelected(){
        return PlayersForMatch.getInstance().getPlayerList().size()>1 && PlayersForMatch.getInstance().getGame() != null;
    }

    public void populateSpinner(String jsonData){
        ObjectMapper mapper = new ObjectMapper();
        try {
            gameList = mapper.readValue(jsonData, mapper.getTypeFactory().constructCollectionType(ArrayList.class, Game.class));
            /****
             *
             */
            List <String> gameNames = new ArrayList<>();
            for(int i =0; i<gameList.size(); i++){
                gameNames.add(gameList.get(i).getNombre());
            }
            gameAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, gameNames);
            gameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            gameSpinner.setAdapter(gameAdapter);

            gameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String nombre = (String) parent.getItemAtPosition(position);
                    Game game = new Game();
                    game.setNombre(nombre);
                    PlayersForMatch.getInstance().setGame(game);
                    Log.d("SPINNER", "***item selected***");
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void populateRecyclerView(String jsonData){
        ObjectMapper mapper = new ObjectMapper();
        try {
            //playerList = mapper.readValue(jsonData, ArrayList.class);
            playerList = mapper.readValue(jsonData, mapper.getTypeFactory().constructCollectionType(ArrayList.class, Player.class));
            playerAdapter = new FriendsAdapter(playerList);
            recyclerView.setAdapter(playerAdapter);
            playerAdapter.notifyDataSetChanged();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
