package rcmm.pedro.es.game4stats.UI.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.widget.ImageView;

import butterknife.Bind;
import butterknife.ButterKnife;
import rcmm.pedro.es.game4stats.Config.Config;
import rcmm.pedro.es.game4stats.Helper.Thread.PostPlayer;
import rcmm.pedro.es.game4stats.Helper.jsonUtil.jsonUtil;
import rcmm.pedro.es.game4stats.R;
import rcmm.pedro.es.game4stats.UI.Dialogs.NewGameDialog;
import rcmm.pedro.es.game4stats.UI.Dialogs.ScoreDialog;
import rcmm.pedro.es.game4stats.model.Player;
import rcmm.pedro.es.game4stats.model.PlayersForMatch;

public class ProfileFragment extends Fragment  implements View.OnClickListener{

    @Bind(R.id.profileImage) ImageView            imageProfile;
    @Bind(R.id.submitGame)   FloatingActionButton submitFab;
    private Player user;

    public ProfileFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = new Player();
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        user.setToken(sharedPreferences.getString("Token", null));
        PostPlayer postPlayer = new PostPlayer(user, this, getContext());
        postPlayer.execute(Config.getSERVER_ADDRESS() + "GetUser", "yes");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, v);
        submitFab.setOnClickListener(this);
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        /*try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void processHttpResponse (String httpResponse){
        if(httpResponse.equals("noMatchFound") || httpResponse.equals("strangeBehaivour")){
            //// TODO: 23/02/2016 protocolo de borrado de token y vuelta a Mainactivity
        }else{
            user = jsonUtil.jsonToPlayer(httpResponse);
            if(user != null)
                setImageProfile(user.getEncodedImage());
        }
    }

    public void setImageProfile(byte[] encodedImage){
        if(encodedImage.length>1) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodedImage, 0, encodedImage.length);
            imageProfile.setImageBitmap(bitmap);
        }else{
            Log.v("SET IMAGE PROFILE", "null encoded image");
        }

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case (R.id.submitGame):
                NewGameDialog newGameDialog = new NewGameDialog(getActivity());
                newGameDialog.show();
                newGameDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        goToScoreDialog();
                    }
                });
                break;
        }
    }

    public void goToScoreDialog(){
        Log.d("Success", "sucxlkjvsdfsd");
        ScoreDialog scoreDialog = new ScoreDialog(getActivity());
        scoreDialog.show();
    }
}
